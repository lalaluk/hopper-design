import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login02',
  templateUrl: './login02.component.html',
  styleUrls: ['./login02.component.css']
})
export class Login02Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  btnClick() {
    this.route.navigate(['Login03']);
  }

}
