import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Login03Component } from './login03.component';

describe('Login03Component', () => {
  let component: Login03Component;
  let fixture: ComponentFixture<Login03Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Login03Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Login03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
