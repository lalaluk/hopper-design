import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login03',
  templateUrl: './login03.component.html',
  styleUrls: ['./login03.component.css']
})
export class Login03Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  btnClick() {
    this.route.navigate(['Login02']);
  }

  backPage() {
    this.route.navigate(['Login03']);
  }

}
