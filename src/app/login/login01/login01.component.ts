import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login01',
  templateUrl: './login01.component.html',
  styleUrls: ['./login01.component.css']
})
export class Login01Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  newMember(): void {
    this.route.navigate(['Login02']);
  }

  haveMember(): void {
    this.route.navigate(['Login02']);
  }

}
