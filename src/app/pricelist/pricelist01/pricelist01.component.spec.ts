import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pricelist01Component } from './pricelist01.component';

describe('Pricelist01Component', () => {
  let component: Pricelist01Component;
  let fixture: ComponentFixture<Pricelist01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pricelist01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pricelist01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
