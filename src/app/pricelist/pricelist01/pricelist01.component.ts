import { Component, OnInit } from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-pricelist01',
  templateUrl: './pricelist01.component.html',
  styleUrls: ['./pricelist01.component.css']
})
export class Pricelist01Component implements OnInit {
  startPosition: string;
  myControl = new FormControl();
  options: string[] = ['大埔', '小埔', '中埔'];
  filteredOptions: Observable<string[]>;

  infoData = [
    {
      place : '新界',
      serviceTime: '服務時間 07:00-19:00',
      color: '#30D8CB',
      detail: [
        {name:'北區',price:'$30-$40'},
        {name:'沙田區',price:'$50-$70'},
        {name:'元朗區',price:'$50-$70'},
        {name:'屯門區',price:'$50-$70'},
        {name:'荃灣區',price:'$50-$70'},
        {name:'葵青區',price:'$50-$70'},
        {name:'西貢區',price:'$50-$70'},
        {name:'離島區',price:'$50-$70'},
      ]
    },
    {
      place : '九龍',
      color: '#3DBDDF',
      serviceTime: '服務時間 06:00-21:00',
      detail: [
        {name:'九龍城區',price:'$30-$40'},
        {name:'觀塘區',price:'$40-$70'},
        {name:'深水埗區',price:'$40-$70'},
        {name:'黃大仙區',price:'$40-$60'},
        {name:'油尖旺區',price:'$50-$70'},
      ]
    },
    {
      place : '香港',
      color: '#4164D8',
      serviceTime: '服務時間 07:30-20:00',
      detail: [
        {name:'中西區',price:'$30-$40'},
        {name:'東區',price:'$50-$70'},
        {name:'南區',price:'$150-$170'},
        {name:'灣仔區',price:'$250-$270'},
      ]
    },
  ];

  constructor() { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}
