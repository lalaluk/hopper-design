import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notification01Component } from './notification01.component';

describe('Notification01Component', () => {
  let component: Notification01Component;
  let fixture: ComponentFixture<Notification01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notification01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notification01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
