import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notification01',
  templateUrl: './notification01.component.html',
  styleUrls: ['./notification01.component.css']
})
export class Notification01Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  btnClick() {
    this.route.navigate(['Register01']);
  }
}
