import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Register01Component } from './register01.component';

describe('Register01Component', () => {
  let component: Register01Component;
  let fixture: ComponentFixture<Register01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Register01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Register01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
