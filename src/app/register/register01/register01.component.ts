import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register01',
  templateUrl: './register01.component.html',
  styleUrls: ['./register01.component.css']
})
export class Register01Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  btnClick() {
    this.route.navigate(['Register02']);
  }
}
