import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Register02Component } from './register02.component';

describe('Register02Component', () => {
  let component: Register02Component;
  let fixture: ComponentFixture<Register02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Register02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Register02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
