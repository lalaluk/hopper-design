import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register02',
  templateUrl: './register02.component.html',
  styleUrls: ['./register02.component.css']
})
export class Register02Component implements OnInit {

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  btnClick() {
    this.route.navigate(['Notification01']);
  }

  backPage(){
    this.route.navigate(['Register01']);
  }
}
