import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Register01Component } from './register/register01/register01.component';
import { Register02Component } from './register/register02/register02.component';
import { Notification01Component } from './register/notification01/notification01.component';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { Qa01Component } from './qa/qa01/qa01.component';
import { Qa02Component } from './qa/qa02/qa02.component';
import { Qa03Component } from './qa/qa03/qa03.component';
import {MatSelectModule} from '@angular/material/select';
import { Login01Component } from './login/login01/login01.component';
import { Login02Component } from './login/login02/login02.component';
import { Login03Component } from './login/login03/login03.component';
import { Pricelist01Component } from './pricelist/pricelist01/pricelist01.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    Register01Component,
    Register02Component,
    Notification01Component,
    Qa01Component,
    Qa02Component,
    Qa03Component,
    Login01Component,
    Login02Component,
    Login03Component,
    Pricelist01Component,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
