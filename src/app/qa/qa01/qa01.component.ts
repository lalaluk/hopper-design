import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qa01',
  templateUrl: './qa01.component.html',
  styleUrls: ['./qa01.component.css']
})
export class Qa01Component implements OnInit {

  constructor(
    private router:Router,
  ) { }

  ngOnInit() {
  }

  btnClick(){
    this.router.navigate(['Qa02']);
  }

}
