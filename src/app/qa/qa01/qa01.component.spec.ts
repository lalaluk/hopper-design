import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Qa01Component } from './qa01.component';

describe('Qa01Component', () => {
  let component: Qa01Component;
  let fixture: ComponentFixture<Qa01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Qa01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Qa01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
