import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qa02',
  templateUrl: './qa02.component.html',
  styleUrls: ['./qa02.component.css']
})
export class Qa02Component implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
  }

  btnClick(){
    this.router.navigate(['Qa03']);
  }

}
