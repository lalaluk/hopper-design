import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Qa02Component } from './qa02.component';

describe('Qa02Component', () => {
  let component: Qa02Component;
  let fixture: ComponentFixture<Qa02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Qa02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Qa02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
