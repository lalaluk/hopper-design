import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qa03',
  templateUrl: './qa03.component.html',
  styleUrls: ['./qa03.component.css']
})
export class Qa03Component implements OnInit {

  workTimeOptions = [];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    for (let i = 0; i < 24; i++) {
      this.workTimeOptions.push({value:`${('0'+i).slice(-2)}:00`,viewValue:`${('0'+i).slice(-2)}:00`});
      this.workTimeOptions.push({value:`${('0'+i).slice(-2)}:30`,viewValue:`${('0'+i).slice(-2)}:30`});
    }
  }

  btnClick() {
    this.router.navigate(['Qa01']);
  }

}
