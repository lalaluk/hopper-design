import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Qa03Component } from './qa03.component';

describe('Qa03Component', () => {
  let component: Qa03Component;
  let fixture: ComponentFixture<Qa03Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Qa03Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Qa03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
