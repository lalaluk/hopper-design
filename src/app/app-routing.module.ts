import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Register01Component } from './register/register01/register01.component';
import { Register02Component } from './register/register02/register02.component';
import { Notification01Component } from './register/notification01/notification01.component';
import { Qa01Component } from './qa/qa01/qa01.component';
import { Qa02Component } from './qa/qa02/qa02.component';
import { Qa03Component } from './qa/qa03/qa03.component';
import { Login01Component } from './login/login01/login01.component';
import { Login02Component } from './login/login02/login02.component';
import { Login03Component } from './login/login03/login03.component';
import { Pricelist01Component } from './pricelist/pricelist01/pricelist01.component';


const routes: Routes = [
  { path: '', redirectTo: 'Login01', pathMatch: 'full' },
  { path: 'Register01', component: Register01Component },
  { path: 'Register02', component: Register02Component },
  { path: 'Notification01', component: Notification01Component },

  { path: 'Qa01', component: Qa01Component },
  { path: 'Qa02', component: Qa02Component },
  { path: 'Qa03', component: Qa03Component },

  { path: 'Login01', component: Login01Component },
  { path: 'Login02', component: Login02Component },
  { path: 'Login03', component: Login03Component },

  { path: 'Pricelist01', component: Pricelist01Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
